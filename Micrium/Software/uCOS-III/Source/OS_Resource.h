#include <os.h>
#include <stdlib.h>

extern int OS_System_Ceiling;

void OSResourceMutexCreate(OS_MUTEX *p_mutex, CPU_CHAR *p_name, OS_ERR *p_err, int ceiling);

OS_OBJ_QTY OSResourceMutexDel(OS_MUTEX *p_mutex, OS_OPT opt, OS_ERR *p_err);

void OSResourceMutexPend(OS_MUTEX *p_mutex, OS_TICK timeout, OS_OPT opt, CPU_TS *p_ts, OS_ERR *p_err);

void OSResourceMutexPost(OS_MUTEX *p_mutex, OS_OPT opt, OS_ERR *p_err);

void  OS_ResourcePend (OS_STATE pending_on);