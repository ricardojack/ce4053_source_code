#include <os.h>
#include <stdlib.h>
#define max(a, b) ((a) > (b) ? (a) : (b))
#define height(p) ((p) == NULL ? -1 : (p)->height)
#define MEM_QTY 60

/*
************************************************************************************************************************
************************************************************************************************************************
*                                                  D A T A   T Y P E S
************************************************************************************************************************
************************************************************************************************************************
*/
typedef struct avl_list_node{
	OS_TCB *value;
        struct avl_list_node* next;
}avl_list_node;

typedef struct _avl_linkedlist{ 
        avl_list_node* head; 
        avl_list_node* tail;
        int size;
}avl_linkedlist;

typedef struct avl_node{
	int key;
	avl_linkedlist ll;
        int height;
	struct avl_node *left;
	struct avl_node *right;
}avl_node;

typedef struct avl_tree{
	avl_node* root;
        int min;
}avl_tree;

typedef struct stack_node{
	OS_MUTEX* mutex; 
        OS_TCB *blocking_task;
        int owner_original_priority;
        int ceiling;
}stack_node;

typedef struct OS_ResourceStack{
	stack_node* top;
        int size;
}OS_ResourceStack;

/*
************************************************************************************************************************
*                                                  GLOBAL VARIABLES
************************************************************************************************************************
*/
extern OS_MEM *ResourcePartitionPtr;
extern OS_MEM *StackResourcePartitionPtr;
extern OS_MEM *ResourceListPartitionPtr;
extern avl_tree OSResourceTree;
extern OS_ResourceStack OSResourceStack;

/*
************************************************************************************************************************
************************************************************************************************************************
*                   				  FUNTION PROTOTYPES
************************************************************************************************************************
************************************************************************************************************************
*/
/* ================================================================================================================== */
/*                                                 AVL TREE                                                           */
/* ================================================================================================================== */
void createResourcePartition();
void createResourceListPartition();
void createStackResourcePartition();
void release_avl_node(avl_node* node);
avl_node* avl_node_right_rotate(avl_node *root);
avl_node* avl_node_left_rotate(avl_node *root);
avl_node* avl_node_double_right_rotate(avl_node* root);
avl_node* avl_node_double_left_rotate(avl_node* root);
avl_node* create_avl_node(int key, OS_TCB* value);
avl_node* avl_tree_insert_node(avl_node* root, int key, OS_TCB* value, int* done);
void avl_tree_insert(avl_tree* tree, int key, OS_TCB* value);
avl_node* avl_tree_remove_min(avl_tree* tree);
avl_node* avl_tree_remove_node(avl_node* root, int key, int* min, int *done, avl_node** minNode);
avl_list_node* insertResourceNode(avl_linkedlist *ll, int index, OS_TCB* value);
avl_list_node* removeResourceListNode(avl_linkedlist *ll, int index);
/* ================================================================================================================== */
/*                                                 STACK                                                              */
/* ================================================================================================================== */
int isEmptyStack(OS_ResourceStack* s);
int peek(OS_ResourceStack* s);
OS_TCB* peekTCB(OS_ResourceStack* s);
int peekPrio(OS_ResourceStack* s);
void push(OS_ResourceStack* s, OS_MUTEX* item, OS_TCB *blocking_task, int owner_original_priority, int ceiling);        
OS_MUTEX* pop(OS_ResourceStack *s);      
avl_list_node* findResourceNode(avl_linkedlist *ll, int index);

